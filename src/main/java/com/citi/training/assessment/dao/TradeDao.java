package com.citi.training.assessment.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.assessment.model.Trade;

public interface TradeDao extends CrudRepository<Trade, Integer> {}

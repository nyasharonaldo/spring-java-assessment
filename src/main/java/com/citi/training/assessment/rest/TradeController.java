package com.citi.training.assessment.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.rest.TradeController;

/**
 * This is the REST interface for managing Trade records.
 * 
 * <p> This class adheres to RESTful paths and response codes. </p>
 * 
 * @author Nyasha
 * 
 * @see Trade
 *
 */

@RestController
@RequestMapping("/trades")
public class TradeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	
	@Autowired
    private TradeDao tradeDao;
	
	
	/**
	 * Retrieve the trades in the database
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET)
    public Iterable<Trade> findAll() {
        LOG.info("HTTP GET findAll()");
        return tradeDao.findAll();
    }
	
	/**
	 * Find a particular trade record by its numerical id.
	 * @param id The id of the record to find.
	 * @return The Trade object that was found
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Trade findById(@PathVariable int id) {
		LOG.info("HTTP GET findById() id=[" + id + "]");
		return tradeDao.findById(id).get();
	}
	
	/**
	 * Save a trade into the database
	 * @param Trade The details of the trade in json format
	 * @return REST Response Code
	 */
	@RequestMapping(method=RequestMethod.POST)
	public HttpEntity<Trade> save(@RequestBody Trade trade){
		LOG.info("HTTP Post save() trade=["+trade+"]");
		return new ResponseEntity<Trade> (tradeDao.save(trade), 
				HttpStatus.CREATED);
	}
	
	/**
	 * Delete a trade by its numerical id
	 * @param id The id of the record to delete.
	 * @return void
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable int id) {
		LOG.info("HTTP Delete delete(); id=["+id+"]");
		tradeDao.deleteById(id);
	}
	
}

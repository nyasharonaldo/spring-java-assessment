package com.citi.training.assessment.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {	
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);
			
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
    public void getTrade_returnsTrade() {
		int testId = 1;
		String testStock = "TSLA";
		int testBuy = 1;
		double testPrice = 234;
		int testVolume = 65;
        
        /// DO A HTTP POST TO /Trades
        ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trades",
                                             new Trade(testId, testStock, testBuy, testPrice, testVolume),
                                             Trade.class);

        LOG.info("Create Trade response: " + createTradeResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
        assertEquals("Test id should be equal to testId",
                testId, createTradeResponse.getBody().getId());
		assertEquals("Trade stock (ticker) should be equal to testStock",
				testStock, createTradeResponse.getBody().getStock());
		assertEquals("Trade buy should be equal to testBuy",
				testBuy, createTradeResponse.getBody().getBuy());
		assertEquals("Trade price should be equal to testPrice",
                testPrice, createTradeResponse.getBody().getPrice(), 0.000001);
		assertEquals("Trade volume should be equal to testVolume", 
				testVolume, createTradeResponse.getBody().getVolume());
        
      
        /// DO A HTTP GET to /Trades/ID
        ResponseEntity<Trade> findTradebyResponse = restTemplate.getForEntity("/trades/"+
        							createTradeResponse.getBody().getId(), Trade.class);
        LOG.info("FindById Response: " + findTradebyResponse.getBody());
        assertEquals(HttpStatus.OK, findTradebyResponse.getStatusCode());
        assertEquals("Found Test id should be equal to testId",
                testId, findTradebyResponse.getBody().getId());
		assertEquals("Found Trade stock (ticker) should be equal to testStock",
				testStock, findTradebyResponse.getBody().getStock());
		assertEquals("Found Trade buy should be equal to testBuy",
				testBuy, findTradebyResponse.getBody().getBuy());
		assertEquals("Found Trade price should be equal to testPrice",
                testPrice, findTradebyResponse.getBody().getPrice(), 0.000001);
		assertEquals("Found Trade volume should be equal to testVolume", 
				testVolume, findTradebyResponse.getBody().getVolume());
	}
	
	@Test
    public void deleteStock(){
        
        /// DO A HTTP DELETE TO /trades/ID
        LOG.info("DeleteById");
        restTemplate.delete("/trades/2");
    
    }
    
	@Test
    public void employeeNotFoundTest() {
    	
    	ResponseEntity<Trade> findStockbyResponse = restTemplate.getForEntity("/trades/"+
				1, Trade.class);
    	assertEquals(HttpStatus.NOT_FOUND, findStockbyResponse.getStatusCode());
    }
	
	
}

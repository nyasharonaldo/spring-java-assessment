package com.citi.training.assessment.trades;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.citi.training.assessment.model.Trade;

public class TradeTests {

	private int testId = 1;
	private String testStock = "TSLA";
	private int testBuy = 1;
	private double testPrice = 234;
	private int testVolume = 65;
	
	@Test
    public void test_Trade_defaultConstructorAndSetters() {
		Trade testTrade = new Trade();
		 
		testTrade.setId(testId);
		testTrade.setStock(testStock);
		testTrade.setBuy(testBuy);
		testTrade.setPrice(testPrice);
		testTrade.setVolume(testVolume);
		
		assertEquals("Test id should be equal to testId",
                testId, testTrade.getId());
		assertEquals("Trade stock (ticker) should be equal to testStock",
				testStock, testTrade.getStock());
		assertEquals("Trade buy should be equal to testBuy",
				testBuy, testTrade.getBuy());
		assertEquals("Trade price should be equal to testPrice",
                testPrice, testTrade.getPrice(), 0.000001);
		assertEquals("Trade volume should be equal to testVolume", 
				testVolume, testTrade.getVolume());
	}
	
	@Test
    public void test_Stock_fullConstructor() {
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

        assertEquals("Trade Id should be equal to testId",
                     testId, testTrade.getId());
        assertEquals("Trade name should be equal to testName",
                     testStock, testTrade.getStock());
        assertEquals("Trade Buy should be equal to testBuy",
    				 testBuy, testTrade.getBuy());
        assertEquals("Trade Price should be equal to testPrice", 
    			testPrice, testTrade.getPrice(), 0.000001);
        assertEquals("Trade Volume should be equal to testVolume",
        		testVolume, testTrade.getVolume());
    }
	
	@Test
	public void test_Trade_toString() {
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
		String expectedString = "{\"id\":" + testId +
							  ",\"stock\":\""+ testStock +
							  "\",\"buy\":"+ testBuy +
							  ",\"price\":"+ testPrice +
							  ",\"volume\":"+ testVolume +"}";
		assertEquals("ToString method should return the expectedString",
					expectedString, testTrade.toString());
		
	}
	
	
	
}
